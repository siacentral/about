# About SiaCentral

## What is SiaCentral?

[SiaCentral]([https://siacentral.com]) is a project I started recently to help manage multiple hosts through a single interface. The end goal is to be able to login through a centralized web portal view stats or manage hosts.

### Video

+ [Demo - Desktop](desktop.md)
+ [Demo - Mobile](mobile.md)

### Screenshots

+ [Host - Dashboard](assets/host-stats.png)
+ [Host - List](assets/host-list.png)
+ [Host - Detail](assets/host-detail.png)
+ [Host - Update Config](assets/host-config.png)

### Initial Host Management Features

+ Update all config variables
+ Add/Remove/Resize storage folders
+ Shutdown/Restart sia daemon
+ Announce 
+ Get email notifications for the following:
	+ host offline
	+ collateral budget full
	+ storage space full
	+ ip address changed
	+ siad needs update
	+ siad crashed

### Planned Additional Features

+ Track download + upload data usage of siad process
+ Announce with unique domain *.siacentral.com instead of IP. Automatic updates of DNS (builtin no-ip)
+ Dedicated hosting UI

## FAQ

### Will I need to download Sia?

Yes.

This client acts as a centralized data exchange between your host and our server. You will need to have the latest version of Sia installed and synced before you can use our service.

### Can SiaCentral steal my coins?

No.

The SiaCentral server can not access any Sia endpoint directly. The client responds to specific pre-programmed commands only. The source code of the client will be available so all commands and endpoints we access can be seen.

### What information do you store?

For each host we store basic information about the daemon and update it every 5 minutes:

+ ip address
+ block chain synced
+ current block height
+ accepting contracts
+ siad version
+ last block hash
+ host working status
+ difficulty
+ wallet confirmed balance
+ contract compensation
+ number of storage obligations
+ locked collateral
+ risked collateral
+ configured storage price
+ used storage capacity
+ remaining storage 
+ bytes sent/received

We do this so we can provide useful historical data even when your host is offline.

### Can anyone send commands to my host?

No.

When a host is added to our service two keys are generated. All commands issued to your client must be signed by the private key unique to each individual host. If a message is not signed it is ignored and the connection is immediately dropped. SiaCentral does not have access to any host's unique private key, so commands cannot be issued by our servers. In addition all communication between your host and our server is encrypted

### Do I need to forward ports?

Generally no.

Your Sia daemon connects directly to our service over TCP. As long as outbound traffic to siacentral.com is allowed our client should not need any additional ports opened or forwarded.

### Is SiaCentral open source?

Not initially, hopefully in the future.

Initially, we plan to provide the full source code only to the SiaCentral client for transparency about what data is sent/received by connected Sia daemons. 

The web UI and server will remain closed source for the time being. 